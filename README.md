# Crypto Wake

Currently hosted at [cryptowake.xyz](https://cryptowake.xyz).

## How to contribute

The Projects files are located in `files/docs/projects/`. See more details about how to contribute on the [Contributing page](https://cryptowake.xyz/about/contributing/).

## Roadmap

* Website redesign
* IPFS support
* Web3 authentication and editing

## Models

* [Civic Tech Graveyard](https://civictech.guide/graveyard/)
* [DOTCOM SÉANCE](https://www.dotcomseance.com/)
* [ZODIAC.WIKI](https://zodiac.wiki/)

## Open questions

- Should this be migrated to Jeykll with a csv backend? Or a Google doc?
    - Would be easier to maintain the database and uniform layout
