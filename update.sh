#!/bin/bash

set -eu

#export MKDOCS_MATERIAL_VERSION="7.0.0"
export SURFER_VERSION="5.13.5-1"

echo "=> Installing mkdocs-material"
sudo pip3 install mkdocs-material#==${MKDOCS_MATERIAL_VERSION}

echo "=> Installing redoc-cli"
npm install -g redoc-cli

echo "=> Installing surfer"
npm install -g cloudron-surfer@${SURFER_VERSION}

echo "=> Updating Docker Image"
docker build . -t cloudron/docs-ci --build-arg #MKDOCS_MATERIAL_VERSION=$MKDOCS_MATERIAL_VERSION --build-arg SURFER_VERSION=$SURFER_VERSION
docker push cloudron/docs-ci

echo ""
echo "=> Done."
echo "=> To use the new docker image, change the sha in the .gitlab-ci.yml <="
echo ""
