# Welcome

*An archive of inactive, failed, and otherwise deceased crypto projects with lessons worth remembering.*

<div style='color:gray; padding:15px;'>
<span style='font-size:x-large'>A wake is a social gathering associated with death, held before a funeral. Traditionally, a wake involves family and friends keeping watch over the body of the dead person, usually in the home of the deceased. … The wake or the viewing of the body is a prominent part of death rituals in many cultures. It allows one last interaction with the dead, providing a time for the living to express their thoughts and feelings with the deceased. It highlights the idea that the loss is one of a social group affecting that group as a whole, and is a way of honoring the dead.</span> —<a href="https://en.wikipedia.org/wiki/Wake_(ceremony)">Wikipedia</a>
</div>

**[Bitnation](/stories/bitnation/)**

Bitnation sought to provide services generally associated with nation-states on blockchain-based infrastructure, proposing to create an "internet of sovereignty." The Bitnation.co domain is now a cryptocurrency news website.

---

**[Civil](/stories/civil/)**

Civil was a platform seeking to rescue journalism from its doldrums by enabling "a decentralized network of independent publishers, governed by a shared set of ethical standards and an engaged community of news consumers." It collapsed after an unsuccessful token sale.

---

**[ConstitutionDAO](/stories/constitutiondao/)**

ConstitutionDAO was an effort to purchase one of the few surviving copies of the United States Constitution. Over $47 million was raised in this effort, but ultimately the DAO was outbid at an auction. Since then, contributors have been partially refunded with considerable value being lost to Ethereum gas fees. 

---

Please consider [contributing](about/contributing).
