# Contributing

Anyone is welcome to contribute to Crypto Wake.

## General guidelines

Employ a tone of celebration mixed with honest assessment. Humor is welcome. Highlight lessons for future builders.

Projects may still be operating, but they should be meaningfully inactive for at least a year in terms of development, use, and market value. Err on the side of using past tense even if a project is nominally still in existence. Ignore outright scam projects unless they furnish especially interesting lessons. 

Cite source material as much as possible with in-text hyperlinks.

## Entry format

Each entry should have the following sections:

* Dates of activity in *italics*
* Introduction - A brief summary of the project, 1-3 sentences
* Lore - A history of the project, including an explanation of its demise
* Lessons - Distill lessons from the project, with headlines in **bold**
* Links - Useful links about the project, starting with the official "Website," along with resources like a Wikipedia page, a codebase, and comprehensive articles elsewhere. Provide at least one Wayback Machine link to the project's website during its heyday.

## Steps for making an edit

Crypto Wake is hosted on a repository in GitLab, and entries are written in a simple format called [Markdown](https://www.markdownguide.org/cheat-sheet). To contribute, use the following steps:

### Edit an existing Story

* Press the pencil icon in the top-right of the Story's page to visit its source page on GitLab.
* Log into GitLab. You can use various social logins, from Google to GitHub.
* Press "Open in Web IDE." When prompted, press "Fork project." This will create a copy of the project on your account.
* Proceed with "Make and save your changes" below.

### Create a new Story

* Press "Contribute" on the top-right of the website, or go [here](https://gitlab.com/medlabboulder/crypto-wake/)
* Log into GitLab. You can use various social logins, from Google to GitHub.
* Then press "Web IDE" on the main repository page. When prompted, press "Fork project." This will create a copy of the project on your account.
* On the left file listing, navigate to `files > docs > stories` to see a list of existing story pages. Create a new one using the pop-up menu next to the `stories` folder. Follow the naming convention of the other Stories.
* Proceed with "Make and save your changes" below.

### Make and save your changes:

* Make the changes you would like to see, in one file or multiple files.
* To add images, upload files to the `files/docs/img/` folder and reference them in the text with `![CAPTION](/img/FILE.NAME)`
* When you're finished, press the "Create commit..." button under the file listing on the left.
* Enter some text as a commit message that summarizes what you did.
* Choose "Create a new branch" and leave "Start a new merge request" checked, and press "Commit."
* Review the information on the next screen and, to proceed, press "Create merge request." This will submit the changes to us for review. Thank you!
